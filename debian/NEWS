imv (4.2.0-1) unstable; urgency=medium

  The legacy `imvr` symlink to imv-x11 is not installed anymore. Please
  call imv-x11 or imv-wayland directly, or the /usr/libexec/imv/imv script
  to automatically select the correct one. This is what imv.desktop does.

 -- Paride Legovini <paride@debian.org>  Sun, 31 Jan 2021 11:52:27 +0000

imv (4.0.1-1) unstable; urgency=medium

  The imv Debian package used to rename the main 'imv' binary to 'imvr' to
  avoid a file name clash with the 'renameutils' package.
  .
  Starting from upstream version 4.0.0 imv ships two binaries that handle
  Wayland and X11 natively: imv-wayland and imv-x11. To allow seamless usage
  to users upstream provides a /usr/bin/imv wrapper that checks whether a
  Wayland compositor is available before running the appropriate binary.
  .
  The Debian package does not ship the wrapper script, avoiding in this way to
  rename anything. The manpages are treated accordingly.
  .
  An 'imvr' symlink to imv-x11 is provided to avoid breaking existing setups.

 -- Paride Legovini <paride@debian.org>  Sat, 30 Nov 2019 16:24:01 +0000
